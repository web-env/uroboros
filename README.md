# dock-starter

**dock-starter** is a ready-to-run docker environement for web development. It embed the following services:
- Composer
- Node
- Apache
- MySQL
- Adminer
- SASS

## Before to start

Just add an index.php file in public folder

## Running

```
docker-compose up -d   
```

## Services details

[WIP]